;; -*- mode: emacs-lisp -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Layer configuration:
This function should only modify configuration layer settings."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs

   ;; Lazy installation of layers (i.e. layers are installed only when a file
   ;; with a supported type is opened). Possible values are `all', `unused'
   ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
   ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
   ;; lazy install any layer that support lazy installation even the layers
   ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
   ;; installation feature and you have to explicitly list a layer in the
   ;; variable `dotspacemacs-configuration-layers' to install it.
   ;; (default 'unused)
   dotspacemacs-enable-lazy-installation 'unused

   ;; If non-nil then Spacemacs will ask for confirmation before installing
   ;; a layer lazily. (default t)
   dotspacemacs-ask-for-lazy-installation t

   ;; If non-nil layers with lazy install support are lazy installed.
   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '()

   ;; List of configuration layers to load.
   dotspacemacs-configuration-layers
   '(
     ;; ----------------------------------------------------------------
     ;; Example of useful layers you may want to use right away.
     ;; Uncomment some layer names and press `SPC f e R' (Vim style) or
     ;; `M-m f e R' (Emacs style) to install them.
     ;; ----------------------------------------------------------------

     ;;;;;;;; personal layers
     ;;;;
     my-javascript

     ;;;;;;;; official layers

     ;;;; +chat
     ;; erc
     ;; rcirc

     ;;;; +checkers
     (spell-checking :variables
                     spell-checking-enable-by-default nil
                     spell-checking-enable-auto-dictionary t
                     enable-flyspell-auto-completion t)
     syntax-checking

     ;;;; +completion
     (auto-completion :variables
                      auto-completion-return-key-behavior nil
                      auto-completion-tab-key-behavior 'complete
                      auto-completion-complete-with-key-sequence nil
                      auto-completion-complete-with-key-sequence-delay 0.1
                      auto-completion-private-snippets-directory nil
                      ;;
                      auto-completion-enable-snippets-in-popup nil
                      auto-completion-enable-help-tooltip t
                      auto-completion-enable-help-tooltip 'manual
                      auto-completion-enable-sort-by-usage t)
     ;;;helm
     ivy

     ;;;; +emacs
     better-defaults
     (ibuffer :variables
              ibuffer-group-buffers-by 'projects)
     (org :variables org-enable-github-support t
          org-enable-bootstrap-support t
          org-enable-reveal-js-support t
          org-enable-org-journal-support t
          org-projectile-file "TODOs.org")
     ;; semantic
     ;; smex
     typography

     ;;;; +email
     gnus

     ;;;; +filetree
     ;; neotree

     ;;;; +fonts
     (unicode-fonts :variables unicode-fonts-force-multi-color-on-mac t)

     ;;;; +source-control
     git
     github
     ;;;version-control

     ;;;; +lang
     asciidoc
     asm
     bibtex
     (c-c++ :variables
            c-c++-default-mode-for-headers 'c++-mode
            c-c++-enable-clang-support t
            c-c++-enable-clang-format-on-save t
            c-c++-enable-rtags-support t
            ;; c-c++-enable-rtags-support 'no-completion
            c-c++-enable-google-style t
            c-c++-enable-google-newline t
            c-c++-enable-auto-newline t)
     (clojure :variables clojure-enable-fancify-symbols nil)
     ;; common-lisp
     csv
     ;; elixir
     (elm :variables
          elm-reactor-port "3333"
          elm-reactor-address "0.0.0.0"
          elm-sort-imports-on-save t)
     emacs-lisp
     ;; erlang
     ;; (ess :variables
     ;;      ess-enable-smart-equals t)
     (go :variables
         go-use-gometalinter t
         gofmt-command "goimports")
     graphviz
     haskell
     html
     ipython-notebook
     (java :variables
           ;; java-backend 'ensime)
           java-backend 'meghanada)
     (javascript :variables
                 javascript-backend 'tern
                 js-indent-level 2
                 js2-basic-offset 2
                 js2-mode-show-strict-warnings nil
                 js2-mode-show-parse-errors nil
                 js2-strict-trailing-comma-warning nil
                 js2-strict-missing-semi-warning nil
                 js2-missing-semi-one-line-override nil
                 node-add-modules-path t)
     json
     kotlin
     latex
     lua
     (markdown :variables markdown-live-preview-engine 'vmd)
     ocaml
     plantuml
     ;; purescript
     (python :variables
             python-test-runner 'pytest
             python-enable-yapf-format-on-save t
             python-sort-imports-on-save t)
     racket
     rust
     (scala :variables
            scala-indent:use-javadoc-style t
            scala-enable-eldoc t
            scala-auto-insert-asterisk-in-comments t
            scala-use-unicode-arrows nil
            scala-auto-start-ensime t)
     scheme
     shell-scripts
     sql
     swift
     (typescript :variables
                 typescript-fmt-tool 'tide
                 typescript-fmt-on-save t)
     windows-scripts
     yaml

     ;;;; +frameworks
     ;; django
     ;; react

     ;;;; +fun
     emoji
     ;; games
     ;; selectric
     ;; xkcd

     ;;;; +intl
     chinese

     ;;;; +pair-programming
     ;; floobits

     ;;;; +themes
     colors
     ;; themes-megapack
     ;; theming

     ;;;; +tools
     ;; chrome
     command-log
     deft
     docker
     ;; fasd
     ;; finance
     ;;;(geolocation :variables
     ;;;             geolocation-enable-automatic-theme-changer t
     ;;;             geolocation-enable-location-service t
     ;;;             geolocation-enable-weather-forecast t)
     imenu-list
     lsp
     nginx
     ;; pandoc
     ;; pdf-tools
     ;; prodigy
     (ranger :variables
             ranger-show-preview t)
     ;; rebox ;; -> cause hang when type space after a /*
     (restclient :variables
                 restclient-use-org t)
     (shell :variables
            ;; Default shell
            ;; shell-default-shell 'eshell
            ;; shell-default-shell 'shell
            ;; shell-default-shell 'term
            shell-default-shell 'ansi-term
            ;; shell-default-shell 'multi-term
            ;; Set shell for term and ansi-term
            ;; shell-default-term-shell "/usr/bin/zsh"
            ;; First be sure ~/.terminfo is setup correctly by running:
            ;; tic -o ~/.terminfo $TERMINFO/e/eterm-color.ti
            shell-default-term-shell "/usr/bin/fish"
            shell-default-position 'full
            ;; Width of the shell popup buffers
            shell-default-full-span nil
            ;; Disable em-smart in Eshell
            shell-enable-smart-eshell nil
            ;; Protect your Eshell prompt
            shell-protect-eshell-prompt t)
     speed-reading
     ;; systemd
     (tern :variables tern-disable-port-files nil)
     ;; terraform
     tmux
     ;; vagrant
     ;; ycmd

     ;;;; +web-services
     (elfeed :variables
             rmh-elfeed-org-files (list "~/.spacemacs.d/feeds/blogs.org"))
     wakatime
     )

   ;; List of additional packages that will be installed without being
   ;; wrapped in a layer. If you need some configuration for these
   ;; packages, then consider creating a layer. You can also put the
   ;; configuration in `dotspacemacs/user-config'.
   dotspacemacs-additional-packages '(disable-mouse
                                      ;; magithub
                                      ;; exwm
                                      ;; exwm-x
                                      counsel
                                      switch-window
                                      buffer-move
                                      ;; cnfonts
                                      all-the-icons
                                      spaceline-all-the-icons
                                      ;; feature-mode
                                      ac-racer
                                      lsp-rust
                                      lsp-go
                                      ;; flycheck-clojure
                                      android-mode
                                      groovy-mode
                                      ;; slime-js
                                      nodejs-repl
                                      tern-auto-complete
                                      prettier-js
                                      eslint-fix
                                      rjsx-mode
                                      ob-typescript
                                      dart-mode
                                      stylefmt
                                      ;; react-snippets
                                      graphql-mode
                                      sqlup-mode
                                      protobuf-mode
                                      ;; litable
                                      elnode
                                      ;; base16-theme
                                      textile-mode
                                      ;; apib-mode
                                      ;; (pyim-greatdict
                                      ;;  :location (recipe
                                      ;;             :fetcher github
                                      ;;             :repo "tumashu/pyim-greatdict"))
                                      )

   ;; A list of packages that cannot be updated.
   dotspacemacs-frozen-packages '()

   ;; A list of packages that will not be installed and loaded.
   dotspacemacs-excluded-packages '(pangu-spacing
                                    undo-tree
                                    ;; org-plus-contrib
                                    ;; org-bullets
                                    ;; evil-escape
                                    )

   ;; Defines the behaviour of Spacemacs when installing packages.
   ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
   ;; `used-only' installs only explicitly used packages and deletes any unused
   ;; packages as well as their unused dependencies. `used-but-keep-unused'
   ;; installs only the used packages but won't delete unused ones. `all'
   ;; installs *all* packages supported by Spacemacs and never uninstalls them.
   ;; (default is `used-only')
   dotspacemacs-install-packages 'used-only))

(defun dotspacemacs/init ()
  "Initialization:
This function is called at the very beginning of Spacemacs startup,
before layer configuration.
It should only modify the values of Spacemacs settings."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non-nil then enable support for the portable dumper. You'll need
   ;; to compile Emacs 27 from source following the instructions in file
   ;; EXPERIMENTAL.org at to root of the git repository.
   ;; (default nil)
   dotspacemacs-enable-emacs-pdumper nil

   ;; File path pointing to emacs 27.1 executable compiled with support
   ;; for the portable dumper (this is currently the branch pdumper).
   ;; (default "emacs-27.0.50")
   dotspacemacs-emacs-pdumper-executable-file "emacs-27.0.50"

   ;; Name of the Spacemacs dump file. This is the file will be created by the
   ;; portable dumper in the cache directory under dumps sub-directory.
   ;; To load it when starting Emacs add the parameter `--dump-file'
   ;; when invoking Emacs 27.1 executable on the command line, for instance:
   ;;   ./emacs --dump-file=~/.emacs.d/.cache/dumps/spacemacs.pdmp
   ;; (default spacemacs.pdmp)
   dotspacemacs-emacs-dumper-dump-file "spacemacs.pdmp"

   ;; If non-nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t

   ;; Maximum allowed time in seconds to contact an ELPA repository.
   ;; (default 5)
   dotspacemacs-elpa-timeout 5

   ;; Set `gc-cons-threshold' and `gc-cons-percentage' when startup finishes.
   ;; This is an advanced option and should not be changed unless you suspect
   ;; performance issues due to garbage collection operations.
   ;; (default '(100000000 0.1))
   dotspacemacs-gc-cons '(100000000 0.1)

   ;; If non-nil then Spacelpa repository is the primary source to install
   ;; a locked version of packages. If nil then Spacemacs will install the
   ;; latest version of packages from MELPA. (default nil)
   dotspacemacs-use-spacelpa nil

   ;; If non-nil then verify the signature for downloaded Spacelpa archives.
   ;; (default nil)
   dotspacemacs-verify-spacelpa-archives nil

   ;; If non-nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil

   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'. (default 'emacs-version)
   dotspacemacs-elpa-subdirectory 'emacs-version

   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style 'emacs

   ;; If non-nil output loading progress in `*Messages*' buffer. (default nil)
   dotspacemacs-verbose-loading nil

   ;; If non-nil then Spacemacs will import your PATH and environment variables
   ;; from your default shell on startup. This is enabled by default for macOS
   ;; users and X11 users.
   dotspacemacs-import-env-vars-from-shell (and (display-graphic-p)
                                                (or (eq system-type 'darwin)
                                                    (eq system-type 'gnu/linux)
                                                    (eq window-system 'x)))

   ;; If nil then use the default shell is used to fetch the environment
   ;; variables. Set this variable to a different shell executable path to
   ;; import the environment variables from this shell. Note that
   ;; `file-shell-name' is preserved and always points to the default shell. For
   ;; instance to use your fish shell environment variables set this variable to
   ;; `/usr/local/bin/fish'.
   ;; (default nil)
   dotspacemacs-import-env-vars-shell-file-name nil

   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner nil

   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `bookmarks' `projects' `agenda' `todos'.
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   dotspacemacs-startup-lists '((recents . 5)
                                (projects . 7))

   ;; True if the home buffer should respond to resize events. (default t)
   dotspacemacs-startup-buffer-responsive t

   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'text-mode

   ;; Initial message in the scratch buffer, such as "Welcome to Spacemacs!"
   ;; (default nil)
   dotspacemacs-initial-scratch-message nil

   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press `SPC T n' to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(solarized-dark
                         zenburn
                         hc-zenburn
                         base16-woodland
                         base16-mocha
                         spacemacs-dark
                         spacemacs-light
                         solarized-light
                         leuven
                         monokai)

   ;; Set the theme for the Spaceline. Supported themes are `spacemacs',
   ;; `all-the-icons', `custom', `vim-powerline' and `vanilla'. The first three
   ;; are spaceline themes. `vanilla' is default Emacs mode-line. `custom' is a
   ;; user defined themes, refer to the DOCUMENTATION.org for more info on how
   ;; to create your own spaceline theme. Value can be a symbol or list with\
   ;; additional properties.
   ;; (default '(spacemacs :separator wave :separator-scale 1.5))
   dotspacemacs-mode-line-theme '(spacemacs :separator wave :separator-scale 1.5)

   ;; If non-nil the cursor color matches the state color in GUI Emacs.
   ;; (default t)
   dotspacemacs-colorize-cursor-according-to-state t

   ;; Default font, or prioritized list of fonts. `powerline-scale' allows to
   ;; quickly tweak the mode-line size to make separators look not too crappy.
   ;; dotspacemacs-default-font '("Source Code Pro"
   ;; dotspacemacs-default-font '("DejaVu Sans Mono"
   ;; dotspacemacs-default-font '("WenQuanYi Micro Hei Mono"
   dotspacemacs-default-font '("DejaVu Sans Mono Nerd Font"
                               :size 16
                               :weight normal
                               :width normal
                               :powerline-scale 1.1)

   ;; The leader key (default "SPC")
   dotspacemacs-leader-key "SPC"

   ;; The key used for Emacs commands `M-x' (after pressing on the leader key).
   ;; (default "SPC")
   dotspacemacs-emacs-command-key "SPC"

   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"

   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"

   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","

   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m")
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"

   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs `C-i', `TAB' and `C-m', `RET'.
   ;; Setting it to a non-nil value, allows for separate commands under `C-i'
   ;; and TAB or `C-m' and `RET'.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab nil

   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"

   ;; If non-nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil

   ;; If non-nil then the last auto saved layouts are resumed automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts nil

   ;; If non-nil, auto-generate layout name when creating new layouts. Only has
   ;; effect when using the "jump to layout by number" commands. (default nil)
   dotspacemacs-auto-generate-layout-names nil

   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1

   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache

   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5

   ;; If non-nil, the paste transient-state is enabled. While enabled, pressing
   ;; `p' several times cycles through the elements in the `kill-ring'.
   ;; (default nil)
   dotspacemacs-enable-paste-transient-state nil

   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.1

   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom

   ;; Control where `switch-to-buffer' displays the buffer. If nil,
   ;; `switch-to-buffer' displays the buffer in the current window even if
   ;; another same-purpose window is available. If non-nil, `switch-to-buffer'
   ;; displays the buffer in a same-purpose window even if the buffer can be
   ;; displayed in the current window. (default nil)
   dotspacemacs-switch-to-buffer-prefers-purpose nil

   ;; If non-nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar t

   ;; If non-nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil

   ;; If non-nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil

   ;; If non-nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup nil

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 90

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 90

   ;; If non-nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t

   ;; If non-nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t

   ;; If non-nil unicode symbols are displayed in the mode line.
   ;; If you use Emacs as a daemon and wants unicode characters only in GUI set
   ;; the value to quoted `display-graphic-p'. (default t)
   dotspacemacs-mode-line-unicode-symbols t

   ;; If non-nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling nil

   ;; Control line numbers activation.
   ;; If set to `t' or `relative' line numbers are turned on in all `prog-mode' and
   ;; `text-mode' derivatives. If set to `relative', line numbers are relative.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; (default nil)
   dotspacemacs-line-numbers nil

   ;; Code folding method. Possible values are `evil' and `origami'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'origami

   ;; If non-nil `smartparens-strict-mode' will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode t

   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc…
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil

   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all

   ;; If non-nil, start an Emacs server if one is not already running.
   ;; (default nil)
   dotspacemacs-enable-server nil

   ;; Set the emacs server socket location.
   ;; If nil, uses whatever the Emacs default is, otherwise a directory path
   ;; like \"~/.emacs.d/server\". It has no effect if
   ;; `dotspacemacs-enable-server' is nil.
   ;; (default nil)
   dotspacemacs-server-socket-dir nil

   ;; If non-nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil

   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `rg', `ag', `pt', `ack' and `grep'.
   ;; (default '("rg" "ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("rg" "ag" "pt" "ack" "grep")

   ;; Format specification for setting the frame title.
   ;; %a - the `abbreviated-file-name', or `buffer-name'
   ;; %t - `projectile-project-name'
   ;; %I - `invocation-name'
   ;; %S - `system-name'
   ;; %U - contents of $USER
   ;; %b - buffer name
   ;; %f - visited file name
   ;; %F - frame name
   ;; %s - process status
   ;; %p - percent of buffer above top of window, or Top, Bot or All
   ;; %P - percent of buffer above bottom of window, perhaps plus Top, or Bot or All
   ;; %m - mode name
   ;; %n - Narrow if appropriate
   ;; %z - mnemonics of buffer, terminal, and keyboard coding systems
   ;; %Z - like %z, but including the end-of-line format
   ;; (default "%I@%S")
   dotspacemacs-frame-title-format "%I@%S"

   ;; Format specification for setting the icon title format
   ;; (default nil - same as frame-title-format)
   dotspacemacs-icon-title-format nil

   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed' to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup 'trailing

   ;; Either nil or a number of seconds. If non-nil zone out after the specified
   ;; number of seconds. (default nil)
   dotspacemacs-zone-out-when-idle nil

   ;; Run `spacemacs/prettify-org-buffer' when
   ;; visiting README.org files of Spacemacs.
   ;; (default nil)
   dotspacemacs-pretty-docs nil))

(defun dotspacemacs/user-init ()
  "Initialization for user code:
This function is called immediately after `dotspacemacs/init', before layer
configuration.
It is mostly for variables that should be set before packages are loaded.
If you are unsure, try setting them in `dotspacemacs/user-config' first."

  (defvar DOT_DIR (concat (getenv "HOME") "/.spacemacs.d"))

  ;; proxy
  (setq url-proxy-services
        '(
          ("http"     . "127.0.0.1:7777")
          ("https"    . "127.0.0.1:7777")
          ("ftp"      . "127.0.0.1:7777")
          ("no_proxy" . "^\\(localhost\\|10.*\\|127.*\\|192.168.*\\)")))

  ;; (setq configuration-layer--elpa-archives
  ;;       '(("gnu"   . "http://elpa.emacs-china.org/gnu/")
  ;;         ("melpa" . "http://elpa.emacs-china.org/melpa/")
  ;;         ("org"   . "http://elpa.emacs-china.org/org/")))

  ;; workaround of issue:
  ;; https://github.com/syl20bnr/spacemacs/issues/7352
  ;; https://github.com/justbur/emacs-which-key/issues/146
  ;; (defalias 'display-buffer-in-major-side-window 'window--make-major-side-window)
  ;; (defalias 'window--make-major-side-window 'display-buffer-in-major-side-window)

  ;; rust -- seems no need anymore
  ;; (setq-default rust-enable-racer t)

  ;; https://github.com/syl20bnr/spacemacs/issues/5803
  ;; (defun spacemacs-buffer/goto-buffer ()
  ;;   (interactive))
  )

(defun dotspacemacs/user-load ()
  "Library to load while dumping.
This function is called while dumping Spacemacs configuration. You can
`require' or `load' the libraries of your choice that will be included
in the dump."
  )

(defun dotspacemacs/user-config ()
  "Configuration for user code:
This function is called at the very end of Spacemacs startup, after layer
configuration.
Put your configuration code here, except for variables that should be set
before packages are loaded."

  ;;;;;;;;;;;;;;;;;;;;;
  ;; common settings ;;
  ;;;;;;;;;;;;;;;;;;;;;

  ;;
  (setq powerline-default-separator 'nil)

  ;; exec-path-from-shell
  (eval-after-load 'exec-path-from-shell
    '(progn
       (setq exec-path-from-shell-arguments '("-l"))
       (when (memq window-system '(mac ns x))
         (exec-path-from-shell-initialize))
       (exec-path-from-shell-copy-env "PYTHONPATH")
       (exec-path-from-shell-copy-env "RUST_SRC_PATH")))

  (setenv "PATH" (concat DOT_DIR "/bin:" (getenv "PATH")))
  (add-to-list 'exec-path (concat DOT_DIR "/bin"))

  (add-to-list 'auto-mode-alist '("\\.?\\(bashrc\\|zshrc\\|shellrc\\|bash_profile\\)" . sh-mode))
  (add-to-list 'auto-mode-alist '("\\.?\\(eslintrc\\)" . json-mode))

  ;; no startup
  ;; (setq inhibit-startup-screen t)
  ;; comments in empty lines.
  ;; (setq comment-empty-lines t)
  ;; no #autosave#.
  ;; (setq auto-save-default nil)
  ;; no backup~.
  ;; (setq make-backup-files nil)
  ;; (setq-default tab-width 4)
  ;; (setq-default indent-tabs-mode nil)

  ;; no automatic indentation
  (electric-indent-mode -1)

  ;; DeleteSelectionMode
  ;; (delete-selection-mode 1)

  ;; AutoFillMode
  ;; (auto-fill-mode t)
  ;; (setq-default fill-column 80)
  ;; (setq-default auto-fill-function 'do-auto-fill)

  ;; FillColumnIndicator
  (setq fci-rule-width 1)
  (setq fci-rule-column 80)
  ;; (setq fci-rule-color "???")
  (define-globalized-minor-mode
    global-fci-mode fci-mode (lambda () (fci-mode t)))
  (add-hook 'prog-mode-hook (lambda () (fci-mode t)))

  ;; WhiteSpace
  (global-whitespace-mode t)
  (setq whitespace-line-column 100)
  (setq whitespace-style '(face trailing tabs spaces lines-tail empty))
  (setq whitespace-global-modes '(not go-mode))
  (setq whitespace-global-modes '(not makefile-mode))
  ;; (defun my-change-whitespace-style ()
  ;;   (progn
  ;;     (setq tab-face (make-face 'tab-face))
  ;;     (set-face-background 'tab-face "blue")
  ;;     (setq whitespace-tab 'tab-face)))
  ;; (add-hook 'go-mode-hook 'my-change-whitespace-style)
  ;; (add-hook 'makefile-mode-hook 'my-change-whitespace-style)

  ;; Issue
  ;; Auto paste into the files opened by mouse-click the item below Recent Files in Spacemacs buffer
  ;; https://github.com/syl20bnr/spacemacs/issues/5435
  (add-hook 'spacemacs-buffer-mode-hook
            (lambda ()
              (set (make-local-variable 'mouse-1-click-follows-link) nil)))

  ;; manage window
  ;; set Ctrl+z do nothing on GUI
  ;; this can also to avoid it to crash emacs on Mac
  (if window-system (global-unset-key (kbd "C-z")))
  ;; maximize window on startup when using a GUI
  (global-set-key (kbd "<f11>") 'toggle-frame-fullscreen)
  (global-set-key (kbd "<f12>") 'toggle-frame-maximized)

  ;; layers/tools/shell
  (add-hook 'term-mode-hook 'toggle-truncate-lines)
  ;; (setq multi-term-program "/bin/bash")
  ;; (setq multi-term-program "/usr/bin/zsh")
  ;; (setq multi-term-program "/usr/bin/fish")
  ;;
  ;; make line-mode the default instead of char-mode in term modes
  ;; https://emacs.stackexchange.com/questions/17289/in-term-mode-how-can-i-make-line-mode-the-default
  ;;;(defun my-enable-term-line-mode (&rest ignored) (term-line-mode))
  ;;;(advice-add 'term :after #'my-enable-term-line-mode)
  ;;;(advice-add 'ansi-term :after #'my-enable-term-line-mode)
  ;;;(advice-add 'multi-term :after #'my-enable-term-line-mode)
  ;;
  ;; set the shell when type "M-x shell"
  ;; (setq shell-file-name "/bin/bash")
  ;; (setq shell-file-name "/usr/bin/zsh")
  ;; (setq shell-file-name "/usr/bin/fish")
  ;; do not split window when run M-x shell
  ;; https://github.com/syl20bnr/spacemacs/issues/6820
  (push (cons "\\*shell\\*" display-buffer--same-window-action)
        display-buffer-alist)

  ;; disable mouse https://github.com/purcell/disable-mouse
  (require 'disable-mouse)
  (global-disable-mouse-mode)

  ;; ;; exwm
  ;; (require 'exwm)
  ;; (require 'exwm-config)
  ;; (exwm-config-default)
  ;; (exwm-input-set-key (kbd "s-&")
  ;;                     (lambda (command)
  ;;                       (interactive (list (read-shell-command "$ ")))
  ;;                       (start-process-shell-command command nil command)))
  ;; (require 'exwm-randr)
  ;; (setq exwm-randr-workspace-output-plist '(0 "HDMI-1"))
  ;; (add-hook 'exwm-randr-screen-change-hook
  ;;           (lambda ()
  ;;             (start-process-shell-command
  ;;              "xrandr" nil "xrandr --output HDMI-1 --auto --output eDP-1 --off")))
  ;; (exwm-randr-enable)
  ;; ;;;(exwm-cm-enable)
  ;; ;;;(exwm-systemtray-enable)
  ;; ;; exwm key bindings
  ;; ;; window move, s-h/j/k/l
  ;; (exwm-input-set-key (kbd "s-h") 'windmove-left)
  ;; (exwm-input-set-key (kbd "s-j") 'windmove-down)
  ;; (exwm-input-set-key (kbd "s-k") 'windmove-up)
  ;; (exwm-input-set-key (kbd "s-l") 'windmove-right)
  ;; ;; buffer move, use https://github.com/lukhas/buffer-move
  ;; (exwm-input-set-key (kbd "S-s-h")     'buf-move-left)
  ;; (exwm-input-set-key (kbd "S-s-j")   'buf-move-down)
  ;; (exwm-input-set-key (kbd "S-s-k")   'buf-move-up)
  ;; (exwm-input-set-key (kbd "S-s-l")  'buf-move-right)
  ;; (exwm-input-set-key (kbd "C-s-h")     'buf-move-left)
  ;; (exwm-input-set-key (kbd "C-s-j")   'buf-move-down)
  ;; (exwm-input-set-key (kbd "C-s-k")   'buf-move-up)
  ;; (exwm-input-set-key (kbd "C-s-l")  'buf-move-right)
  ;; (exwm-input-set-key (kbd "M-s-h")     'buf-move-left)
  ;; (exwm-input-set-key (kbd "M-s-j")   'buf-move-down)
  ;; (exwm-input-set-key (kbd "M-s-k")   'buf-move-up)
  ;; (exwm-input-set-key (kbd "M-s-l")  'buf-move-right)
  ;; ;; window move, M-left/right/up/down
  ;; (exwm-input-set-key [M-left] 'windmove-left)
  ;; (exwm-input-set-key [M-right] 'windmove-right)
  ;; (exwm-input-set-key [M-up] 'windmove-up)
  ;; (exwm-input-set-key [M-down] 'windmove-down)
  ;; ;; buffer move, s-left/right/up/down
  ;; (exwm-input-set-key [s-left] 'buf-move-left)
  ;; (exwm-input-set-key [s-right] 'buf-move-right)
  ;; (exwm-input-set-key [s-up] 'buf-move-up)
  ;; (exwm-input-set-key [s-down] 'buf-move-down)
  ;; (exwm-input-set-key [S-s-left] 'buf-move-left)
  ;; (exwm-input-set-key [S-s-right] 'buf-move-right)
  ;; (exwm-input-set-key [S-s-up] 'buf-move-up)
  ;; (exwm-input-set-key [S-s-down] 'buf-move-down)
  ;; (exwm-input-set-key [C-s-left] 'buf-move-left)
  ;; (exwm-input-set-key [C-s-right] 'buf-move-right)
  ;; (exwm-input-set-key [C-s-up] 'buf-move-up)
  ;; (exwm-input-set-key [C-s-down] 'buf-move-down)
  ;; (exwm-input-set-key [M-s-left] 'buf-move-left)
  ;; (exwm-input-set-key [M-s-right] 'buf-move-right)
  ;; (exwm-input-set-key [M-s-up] 'buf-move-up)
  ;; (exwm-input-set-key [M-s-down] 'buf-move-down)
  ;; ;; loop key
  ;; (defun prev-window () (interactive) (other-window -1))
  ;; (exwm-input-set-key [M-tab] 'other-window)
  ;; (exwm-input-set-key [C-tab] 'prev-window)
  ;; (exwm-input-set-key (kbd "C-.") 'other-window)
  ;; (exwm-input-set-key (kbd "C-,") 'prev-window)

  ;; switch-window
  ;; https://github.com/dimitri/switch-window
  (global-set-key (kbd "C-x o") 'switch-window)
  (global-set-key (kbd "C-x 1") 'switch-window-then-maximize)
  (global-set-key (kbd "C-x 2") 'switch-window-then-split-below)
  (global-set-key (kbd "C-x 3") 'switch-window-then-split-right)
  (global-set-key (kbd "C-x 0") 'switch-window-then-delete)
  ;; I want to select a window with "a-z" instead of "1-9".
  (setq switch-window-shortcut-style 'qwerty)
  (setq switch-window-qwerty-shortcuts
        '("a" "s" "d" "f" "j" "k" "l" ";" "w" "e" "i" "o"))
  ;; I want to hide window label when window's number < 3
  (setq switch-window-threshold 2)
  ;; I want to select minibuffer with label "z".
  (setq switch-window-minibuffer-shortcut ?z)
  ;; Switch-window seem to conflict with Exwm, how to do?
  (setq switch-window-input-style 'minibuffer)
  ;; I use text terminal, but I want bigger label.
  ;;;(setq switch-window-shortcut-appearance 'asciiart)

  ;; cnfonts
  ;;https://github.com/tumashu/cnfonts
  ;;;(cnfonts-enable)
  ;;;(setq cnfonts-profiles-directory (concat DOT_DIR "/cnfonts"))
  ;;;(setq cnfonts-profiles '("default"))
  ;;;(cnfonts-set-spacemacs-fallback-fonts)

  ;;;;;;;;;;;;;;;;;;;;;
  ;; manual settings ;;
  ;;;;;;;;;;;;;;;;;;;;;

  ;; (add-to-list 'load-path (concat DOT_DIR "/emacs-lisp"))
  (let ((basedir (concat DOT_DIR "/emacs-lisp")))
    (add-to-list 'load-path basedir)
    (dolist (f (directory-files basedir))
      (if (and (not (or (equal f ".") (equal f "..")))
               (file-directory-p (concat basedir f)))
          (add-to-list 'load-path (concat basedir f)))))

  ;; pyim
  ;; (require 'pyim)
  ;; (require 'pyim-greatdict)
  ;; (pyim-greatdict-enable)
  ;; (setq default-input-method "pyim")

  ;; linum+
  (require 'linum+)
  ;; (global-linum-mode t)
  (global-set-key (kbd "<f5>") 'global-linum-mode)
  (global-set-key (kbd "<f6>") 'linum-mode)
  ;; hooks for automatic enable linum-mode
  (add-hook 'prog-mode-hook 'linum-mode)
  (add-hook 'text-mode-hook 'linum-mode)
  ;; gyp
  (require 'gyp)
  ;; enable yasnippet globally
  (yas-global-mode t)
  (add-hook 'prog-mode-hook (lambda () (yas-reload-all)))
  (add-hook 'prog-mode-hook (lambda () (yas/minor-mode-on)))
  (add-hook 'prog-mode-hook #'yas-minor-mode)
  ;; use M-x yas-reload-all to activate them
  (yas-reload-all)

  ;; all-the-icons
  (require 'all-the-icons)
  (require 'spaceline-all-the-icons)
  ;;(use-package spaceline-all-the-icons
  ;;  :after spaceline
  ;;  :config (spaceline-all-the-icons-theme))
  (spaceline-all-the-icons--setup-anzu)            ;; Enable anzu searching
  ;;(spaceline-all-the-icons--setup-package-updates) ;; Enable package update indicator
  (spaceline-all-the-icons--setup-git-ahead)       ;; Enable # of commits ahead of upstream in git
  (spaceline-all-the-icons--setup-paradox)         ;; Enable Paradox mode line
  ;; (spaceline-all-the-icons--setup-neotree)         ;; Enable Neotree mode line

  ;; magithub https://github.com/vermiculus/magithub
  ;; ghub https://github.com/magit/ghub
  ;; (use-package magithub
  ;;   :after magit
  ;;   :config
  ;;   (magithub-feature-autoinject t)
  ;;   (setq magithub-clone-default-directory "~/github"))

  ;; anzu
  (global-anzu-mode +1)
  (global-set-key [remap query-replace] 'anzu-query-replace)
  (global-set-key [remap query-replace-regexp] 'anzu-query-replace-regexp)

  ;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; programming settings ;;
  ;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; https://github.com/Malabarba/aggressive-indent-mode
  ;; (add-hook 'prog-mode-hook #'aggressive-indent-mode)

  ;; don't hide tooltip automatically
  (setq flycheck-pos-tip-timeout -1)

  (global-set-key (kbd "C-c TAB") 'yas-expand)
  (spacemacs/toggle-indent-guide-globally-on)

  ;; smartparens
  (add-hook 'emacs-lisp-mode-hook #'smartparens-strict-mode)
  (add-hook 'clojure-mode-hook #'smartparens-strict-mode)

  ;; feature-mode for cucumber (BDD)
  (setq feature-default-language "fi")
  (add-to-list 'auto-mode-alist '("\.feature$" . feature-mode))

  ;; neotree
  ;; (setq neo-smart-open t)
  ;; (setq neo-theme (if (display-graphic-p) 'icons 'arrow))

  ;; org-mode
  (with-eval-after-load 'org-agenda
    (require 'org-projectile)
    (push (org-projectile:todo-files) org-agenda-files))
  (with-eval-after-load 'org
    (add-hook 'org-mode-hook #'visual-line-mode)
    ;; for line break on chinese characters
    (add-hook 'org-mode-hook
              (lambda () (setq truncate-lines nil)))
    (setq org-latex-pdf-process '("xelatex -interaction nonstopmode %f"
                                  "xelatex -interaction nonstopmode %f"))
    ;; eval code without confirm
    (setq org-confirm-babel-evaluate nil)
    (setq org-directory "~/org/captures/"
          org-journal-dir "~/org/journal/"
          org-journal-file-format "%Y-%m-%d"
          org-journal-date-prefix "#+TITLE: "
          org-journal-date-format "%A, %B %d %Y"
          org-journal-time-prefix "* "
          org-journal-time-format ""
          org-startup-indented t
          org-bullets-bullet-list '("■" "◆" "▲" "▶")
          spaceline-org-clock-p t)
    (global-set-key "\C-cl" 'org-store-link)
    (global-set-key "\C-ca" 'org-agenda)
    (global-set-key "\C-cc" 'org-capture)
    (global-set-key "\C-cb" 'org-iswitchb)
    (org-babel-do-load-languages
     'org-babel-load-languages
     '(;; Core
       (awk . t)
       (emacs-lisp . t)
       (C . t)
       ;; (cpp . t)
       (clojure . t)
       (haskell . t)
       (java . t)
       (js . t)
       (latex . t)
       (lisp . t)
       (makefile . t)
       (ocaml . t)
       (org . t)
       (python . t)
       ;; (R . t)
       (scala . t)
       (scheme . t)
       (sed . t)
       (shell . t)
       (sql . t)
       (sqlite . t)
       ;; Emacs Package
       (http . t)
       (typescript . t)
       (dot . t)
       (plantuml . t))))

  ;; chrome
  ;; (defun my-flymd-browser-function (url)
  ;;   (let ((browse-url-browser-function 'browse-url-firefox))
  ;;     (browse-url url)))
  ;; (setq flymd-browser-open-function 'my-flymd-browser-function)

  ;; shell
  (add-hook 'term-mode-hook 'toggle-truncate-lines)

  ;; git
  (global-set-key (kbd "C-x g") 'magit-status)
  (global-set-key (kbd "C-x M-g") 'magit-dispatch-popup)

  ;; c-c++
  ;; Bind clang-format-region to C-M-tab in all modes:
  (global-set-key [C-M-tab] 'clang-format-region)
  ;; Bind clang-format-buffer to tab on the c++-mode only:
  (add-hook 'c-mode-hook 'clang-format-bindings)
  (add-hook 'c++-mode-hook 'clang-format-bindings)
  (defun clang-format-bindings ()
    (local-set-key [tab] 'clang-format-buffer))
  ;; (define-key c++-mode-map [tab] 'clang-format-buffer))

  ;; go
  ;; lsp-go
  (add-hook 'go-mode-hook #'lsp-go-enable)

  ;; rust
  (with-eval-after-load 'rust-mode
    ;; racer
    (unless (getenv "RUST_SRC_PATH")
      (setenv "RUST_SRC_PATH" (expand-file-name "~/.multirust/toolchains/nightly-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src")))
    (setenv "EXTERN_C_DIR" "target/extern_C")
    (add-hook 'rust-mode-hook #'aggressive-indent-mode)
    (add-hook 'rust-mode-hook #'racer-mode)
    (add-hook 'racer-mode-hook #'eldoc-mode)
    ;; company
    (add-hook 'racer-mode-hook #'company-mode)
    (require 'rust-mode)
    (define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)
    ;; (add-hook 'rust-mode-hook 'rust-indent-bindings)
    ;; (defun rust-indent-bindings ()
    ;;   (local-set-key rust-mode-map [tab] #'company-indent-or-complete-common))
    (setq company-tooltip-align-annotations t)
    ;; rustfmt: to enable automatic buffer formatting on save
    (setq rust-format-on-save t))
  ;; lsp-rust
  (with-eval-after-load 'lsp-mode
    (setq lsp-rust-rls-command '("rustup" "run" "nightly" "rls"))
    (require 'lsp-rust))
  (add-hook 'rust-mode-hook #'lsp-rust-enable)
  (add-hook 'rust-mode-hook #'flycheck-mode)

  ;; clojure
  (add-hook 'clojure-mode-hook #'aggressive-indent-mode)
  (add-hook 'cider-mode-hook #'aggressive-indent-mode)
  (add-hook 'cider-mode-hook 'cider-enlighten-mode)
  (with-eval-after-load 'cider
    ;; (require 'cider)
    (setq cider-cljs-lein-repl
          "(do (require 'figwheel-sidecar.repl-api)
               (figwheel-sidecar.repl-api/start-figwheel!)
               (figwheel-sidecar.repl-api/cljs-repl))"))
  ;; ;; flycheck
  ;; ;; https://github.com/clojure-emacs/squiggly-clojure
  ;; (eval-after-load 'flycheck '(flycheck-clojure-setup))
  ;; (add-hook 'after-init-hook #'global-flycheck-mode)
  ;; (eval-after-load 'flycheck
  ;;   '(setq flycheck-display-errors-function #'flycheck-pos-tip-error-messages))

  ;; scala
  (setq-default flycheck-scalastylerc
                (concat DOT_DIR "/conf/scalastyle_config.xml"))

  ;; java
  ;; (setq eclim-eclipse-dirs '("~/app/eclipse")
  ;;       eclim-executable "~/app/eclipse/eclim"
  ;;       ;; eclimd-default-workspace "/path/to/default/eclipse/workspace"
  ;;       eclimd-wait-for-process t)
  (add-hook 'java-mode-hook
            (lambda ()
              (setq c-basic-offset 2)
              (add-hook 'before-save-hook 'meghanada-code-beautify-before-save)))
  (add-to-list 'auto-mode-alist '("\\.gradle\\'" . groovy-mode))

  ;; javascript
  ;; skewer-mode/livid-mode/simple-httpd port
  (eval-after-load 'simple-httpd
    (setq httpd-port 8666))
  ;; rjsx
  ;; (add-hook 'rjsx-mode-hook 'emmet-mode)
  ;; tern
  (eval-after-load 'tern
    '(progn
       (require 'tern-auto-complete)
       (tern-ac-setup)))
  ;; prettier-js
  ;; (add-hook 'js-mode-hook 'prettier-js-mode)
  ;; (add-hook 'js2-mode-hook 'prettier-js-mode)
  ;; (add-hook 'rjsx-mode-hook 'prettier-js-mode)
  ;; (eval-after-load 'prettier-js
  ;;   '(progn
  ;;      (setq prettier-js-command "prettier-eslint")))
  ;; eslint-fix
  (eval-after-load 'js-mode
    '(add-hook 'js-mode-hook (lambda () (add-hook 'after-save-hook 'eslint-fix nil t))))
  (eval-after-load 'js2-mode
    '(add-hook 'js2-mode-hook (lambda () (add-hook 'after-save-hook 'eslint-fix nil t))))
  (eval-after-load 'rjsx-mode
    '(add-hook 'rjsx-mode-hook (lambda () (add-hook 'after-save-hook 'eslint-fix nil t))))
  ;; js2-highlight-vars-mode
  ;; http://mihai.bazon.net/projects/editing-javascript-with-emacs-js2-mode/js2-highlight-vars-mode
  ;; https://github.com/unhammer/js2-highlight-vars.el
  ;;;(require 'js2-highlight-vars)
  ;;;(add-hook 'js-mode-hook 'js2-highlight-vars-mode)
  ;;;(add-hook 'js2-mode-hook 'js2-highlight-vars-mode)
  ;;;(add-hook 'rjsx-mode-hook 'js2-highlight-vars-mode)

  ;; typescript
  (setq typescript-indent-level 2)
  (setq tide-format-options
        '(:indentSize 2 :tabSize: 2
                      :insertSpaceAfterCommaDelimiter t
                      :insertSpaceAfterSemicolonInForStatements t
                      :insertSpaceBeforeAndAfterBinaryOperators t
                      :insertSpaceAfterKeywordsInControlFlowStatements t
                      :insertSpaceAfterFunctionKeywordForAnonymousFunctions t
                      :insertSpaceAfterOpeningAndBeforeClosingNonemptyParenthesis nil
                      :insertSpaceAfterOpeningAndBeforeClosingNonemptyBrackets nil
                      :insertSpaceAfterOpeningAndBeforeClosingTemplateStringBraces nil
                      :insertSpaceAfterOpeningAndBeforeClosingJsxExpressionBraces nil
                      :placeOpenBraceOnNewLineForFunctions nil
                      :placeOpenBraceOnNewLineForControlBlocks nil))
  (setq tide-tsserver-process-environment '("TSS_LOG=-level verbose -file /tmp/tss.log"))
  ;;;(setq tide-tsserver-executable "node_modules/typescript/bin/tsserver")
  ;; Use typescript-mode instead of web-mode for tsx files in tide
  (add-to-list 'auto-mode-alist '("\\.tsx\\'" . typescript-mode))
  ;; For tsx under typescript-mode
  (add-hook 'typescript-mode-hook 'emmet-mode)

  ;; html-mode/web-mode
  ;; (add-to-list 'auto-mode-alist '("\\.html?\\'" . html-mode))
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  (add-hook 'sgml-mode-hook #'smartparens-strict-mode)
  (add-hook 'html-mode-hook #'smartparens-strict-mode)
  ;; for html formatting, use web-beautify as prettier does not support it.
  ;; (eval-after-load 'sgml-mode
  ;;   '(add-hook 'sgml-mode-hook
  ;;              (lambda ()
  ;;                (add-hook 'before-save-hook 'web-beautify-html-buffer t t))))
  ;; (eval-after-load 'html-mode
  ;;   '(add-hook 'html-mode-hook
  ;;              (lambda ()
  ;;                (add-hook 'before-save-hook 'web-beautify-html-buffer t t))))
  ;; (eval-after-load 'web-mode
  ;;   '(add-hook 'web-mode-hook
  ;;              (lambda ()
  ;;                (add-hook 'before-save-hook 'web-beautify-html-buffer t t))))
  (eval-after-load 'css-mode
    '(progn
       ;;(add-hook 'css-mode-hook
       ;;          (lambda () (add-hook 'before-save-hook 'web-beautify-css-buffer t t)))
       ;; (add-hook 'css-mode-hook 'stylefmt-enable-on-save)
       (setq flycheck-stylelintrc "~/.stylelintrc")
       (add-hook 'css-mode-hook 'flycheck-mode)))
  ;; other web-mode setting
  (with-eval-after-load 'web-mode
    (setq-default
     js2-basic-offset 2
     js-indent-level 2
     css-indent-offset 2
     web-mode-markup-indent-offset 2
     web-mode-css-indent-offset 2
     web-mode-code-indent-offset 2
     web-mode-attr-indent-offset 2)
    (add-to-list 'web-mode-indentation-params '("lineup-args" . nil))
    (add-to-list 'web-mode-indentation-params '("lineup-concats" . nil))
    (add-to-list 'web-mode-indentation-params '("lineup-calls" . nil)))

  ;; sql
  ;; Capitalize keywords in SQL mode
  (add-hook 'sql-mode-hook 'sqlup-mode)
  ;; Capitalize keywords in an interactive session (e.g. psql)
  (add-hook 'sql-interactive-mode-hook 'sqlup-mode)
  ;; Set a global keyword to use sqlup on a region
  (global-set-key (kbd "C-c u") 'sqlup-capitalize-keywords-in-region)

  ;; emacs lisp
  ;; (remove-hook 'emacs-lisp-mode-hook 'auto-compile-mode)
  ;; (add-hook 'emacs-lisp-mode-hook 'litable-mode)

  ;; apib-mode https://github.com/w-vi/apib-mode
  ;; (add-to-list 'auto-mode-alist '("\\.apib\\'" . apib-mode))

  ;; plantuml mode
  (setq plantuml-jar-path "~/.spacemacs.d/bin/plantuml.1.2018.1.jar")
  (setq org-plantuml-jar-path plantuml-jar-path)

  ;; deft
  (with-eval-after-load 'deft
    (setq deft-extensions '("org" "md" "txt"))
    (setq deft-directory "~/Notes"))

  ;; finance
  (setq ledger-post-amount-alignment-column 68)

  ;; geolocation
  ;; (load-file "~/.geolocation.el")

  ;; wakatime api-key
  (with-eval-after-load 'wakatime-mode
    (load-file "~/.wakatime.cfg.el"))

  ;;;;;;;;;;;;;;;;;;;;
  ;; other settings ;;
  ;;;;;;;;;;;;;;;;;;;;

  ;; startup window size
  (when window-system (set-frame-size (selected-frame) 100 32))

  ;; set chinese font
  (dolist (charset '(kana han symbol cjk-misc bopomofo))
    (set-fontset-font (frame-parameter nil 'font)
                      charset (font-spec :family "文泉驿等宽微米黑"
                                         :size 20)))

  ;; end user-config
  )

;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (magit let-alist iedit anzu undo-tree csv-mode inflections edn seq spinner queue auctex wgrep wakatime-mode utop typo tuareg caml swift-mode stylefmt spray smex smartparens ranger pyim-basedict plantuml-mode ox-reveal origami org-ref pdf-tools key-chord org-category-capture alert log4e gntp org-mime ocp-indent ob-typescript merlin lsp-rust lsp-go lsp-mode ivy-hydra intero imenu-list ibuffer-projectile hydra hlint-refactor hindent parent-mode helm-bibtex parsebib haskell-snippets groovy-mode graphviz-dot-mode gitignore-mode git-commit flyspell-popup flyspell-correct-ivy flyspell-correct flycheck-haskell flycheck-elm flycheck flx ghub with-editor evil goto-chg emoji-cheat-sheet-plus helm helm-core elnode db fakir creole web kv elm-mode elfeed-web elfeed-org org-plus-contrib elfeed-goodies ace-jump-mode powerline elfeed skewer-mode request js2-mode simple-httpd json-mode tablist magit-popup docker-tramp json-snatcher json-reformat disable-mouse diminish deft counsel-projectile projectile swiper dash-functional tern restclient know-your-http-well company-ghci company-ghc ghc haskell-mode company-emoji company-emacs-eclim eclim company-cabal company-auctex company command-log-mode cmm-mode multiple-cursors paredit peg eval-sexp-fu highlight cider pkg-info clojure-mode epl markdown-mode bind-map bind-key biblio biblio-core yasnippet auto-dictionary packed async android-mode anaconda-mode pythonic adoc-mode markup-faces avy racer pos-tip f rust-mode s auto-complete popup graphql-mode powershell yaml-mode org-bullets evil-escape racket-mode faceup lua-mode geiser magit-gh-pulls github-search github-clone github-browse-file eslint-fix stickyfunc-enhance srefactor tide typescript-mode go-guru go-eldoc flycheck-gometalinter company-go go-mode buffer-move cnfonts counsel exwm-x ivy exwm xelb noflet ensime sbt-mode scala-mode nodejs-repl gist gh marshal logito pcache ht pyim pangu-spacing find-by-pinyin-dired ace-pinyin pinyinlib pyim-greatdict thrift textile-mode ac-js2 haml-mode web-completion-data react-snippets ein request-deferred websocket deferred spaceline-all-the-icons all-the-icons memoize font-lock+ zenburn-theme yapfify xterm-color x86-lookup ws-butler winum which-key web-mode web-beautify volatile-highlights vmd-mode vi-tilde-fringe uuidgen use-package unfill toml-mode toc-org tern-auto-complete tagedit switch-window sqlup-mode sql-indent spaceline solarized-theme smeargle slim-mode shell-pop scss-mode sass-mode rjsx-mode restclient-helm restart-emacs rainbow-mode rainbow-identifiers rainbow-delimiters pyvenv pytest pyenv-mode py-isort pug-mode protobuf-mode prettier-js popwin pip-requirements persp-mode pcre2el paradox ox-twbs ox-gfm orgit org-projectile org-present org-pomodoro org-download open-junk-file ob-restclient ob-http nginx-mode neotree nasm-mode mwim multi-term move-text monokai-theme mmm-mode markdown-toc magit-gitflow macrostep lorem-ipsum livid-mode live-py-mode linum-relative link-hint less-css-mode js2-refactor js-doc insert-shebang info+ indent-guide hy-mode hungry-delete htmlize hl-todo highlight-parentheses highlight-numbers highlight-indentation hide-comnt help-fns+ helm-themes helm-swoop helm-pydoc helm-projectile helm-mode-manager helm-make helm-gitignore helm-flx helm-descbinds helm-css-scss helm-company helm-c-yasnippet helm-ag hc-zenburn-theme google-translate golden-ratio gnuplot gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link gh-md fuzzy flycheck-rust flycheck-pos-tip flx-ido fish-mode fill-column-indicator fancy-battery eyebrowse expand-region exec-path-from-shell evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-surround evil-search-highlight-persist evil-numbers evil-nerd-commenter evil-mc evil-matchit evil-magit evil-lisp-state evil-indent-plus evil-iedit-state evil-exchange evil-ediff evil-args evil-anzu eslintd-fix eshell-z eshell-prompt-extras esh-help emmet-mode elisp-slime-nav dumb-jump dockerfile-mode docker disaster define-word cython-mode company-web company-tern company-statistics company-shell company-restclient company-quickhelp company-c-headers company-anaconda column-enforce-mode color-identifiers-mode coffee-mode cmake-mode clojure-snippets clj-refactor clean-aindent-mode clang-format cider-eval-sexp-fu cargo auto-yasnippet auto-highlight-symbol auto-compile aggressive-indent adaptive-wrap ace-window ace-link ace-jump-helm-line ac-racer ac-ispell))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(defun dotspacemacs/emacs-custom-settings ()
  "Emacs custom settings.
This is an auto-generated function, do not modify its content directly, use
Emacs customize menu instead.
This function is called at the very end of Spacemacs initialization."
  (custom-set-variables
   ;; custom-set-variables was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   '(package-selected-packages
     '(dart-mode magit let-alist iedit anzu undo-tree csv-mode inflections edn seq spinner queue auctex wgrep wakatime-mode utop typo tuareg caml swift-mode stylefmt spray smex smartparens ranger pyim-basedict plantuml-mode ox-reveal origami org-ref pdf-tools key-chord org-category-capture alert log4e gntp org-mime ocp-indent ob-typescript merlin lsp-rust lsp-go lsp-mode ivy-hydra intero imenu-list ibuffer-projectile hydra hlint-refactor hindent parent-mode helm-bibtex parsebib haskell-snippets groovy-mode graphviz-dot-mode gitignore-mode git-commit flyspell-popup flyspell-correct-ivy flyspell-correct flycheck-haskell flycheck-elm flycheck flx ghub with-editor evil goto-chg emoji-cheat-sheet-plus helm helm-core elnode db fakir creole web kv elm-mode elfeed-web elfeed-org org-plus-contrib elfeed-goodies ace-jump-mode powerline elfeed skewer-mode request js2-mode simple-httpd json-mode tablist magit-popup docker-tramp json-snatcher json-reformat disable-mouse diminish deft counsel-projectile projectile swiper dash-functional tern restclient know-your-http-well company-ghci company-ghc ghc haskell-mode company-emoji company-emacs-eclim eclim company-cabal company-auctex company command-log-mode cmm-mode multiple-cursors paredit peg eval-sexp-fu highlight cider pkg-info clojure-mode epl markdown-mode bind-map bind-key biblio biblio-core yasnippet auto-dictionary packed async android-mode anaconda-mode pythonic adoc-mode markup-faces avy racer pos-tip f rust-mode s auto-complete popup graphql-mode powershell yaml-mode org-bullets evil-escape racket-mode faceup lua-mode geiser magit-gh-pulls github-search github-clone github-browse-file eslint-fix stickyfunc-enhance srefactor tide typescript-mode go-guru go-eldoc flycheck-gometalinter company-go go-mode buffer-move cnfonts counsel exwm-x ivy exwm xelb noflet ensime sbt-mode scala-mode nodejs-repl gist gh marshal logito pcache ht pyim pangu-spacing find-by-pinyin-dired ace-pinyin pinyinlib pyim-greatdict thrift textile-mode ac-js2 haml-mode web-completion-data react-snippets ein request-deferred websocket deferred spaceline-all-the-icons all-the-icons memoize font-lock+ zenburn-theme yapfify xterm-color x86-lookup ws-butler winum which-key web-mode web-beautify volatile-highlights vmd-mode vi-tilde-fringe uuidgen use-package unfill toml-mode toc-org tern-auto-complete tagedit switch-window sqlup-mode sql-indent spaceline solarized-theme smeargle slim-mode shell-pop scss-mode sass-mode rjsx-mode restclient-helm restart-emacs rainbow-mode rainbow-identifiers rainbow-delimiters pyvenv pytest pyenv-mode py-isort pug-mode protobuf-mode prettier-js popwin pip-requirements persp-mode pcre2el paradox ox-twbs ox-gfm orgit org-projectile org-present org-pomodoro org-download open-junk-file ob-restclient ob-http nginx-mode neotree nasm-mode mwim multi-term move-text monokai-theme mmm-mode markdown-toc magit-gitflow macrostep lorem-ipsum livid-mode live-py-mode linum-relative link-hint less-css-mode js2-refactor js-doc insert-shebang info+ indent-guide hy-mode hungry-delete htmlize hl-todo highlight-parentheses highlight-numbers highlight-indentation hide-comnt help-fns+ helm-themes helm-swoop helm-pydoc helm-projectile helm-mode-manager helm-make helm-gitignore helm-flx helm-descbinds helm-css-scss helm-company helm-c-yasnippet helm-ag hc-zenburn-theme google-translate golden-ratio gnuplot gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link gh-md fuzzy flycheck-rust flycheck-pos-tip flx-ido fish-mode fill-column-indicator fancy-battery eyebrowse expand-region exec-path-from-shell evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-surround evil-search-highlight-persist evil-numbers evil-nerd-commenter evil-mc evil-matchit evil-magit evil-lisp-state evil-indent-plus evil-iedit-state evil-exchange evil-ediff evil-args evil-anzu eslintd-fix eshell-z eshell-prompt-extras esh-help emmet-mode elisp-slime-nav dumb-jump dockerfile-mode docker disaster define-word cython-mode company-web company-tern company-statistics company-shell company-restclient company-quickhelp company-c-headers company-anaconda column-enforce-mode color-identifiers-mode coffee-mode cmake-mode clojure-snippets clj-refactor clean-aindent-mode clang-format cider-eval-sexp-fu cargo auto-yasnippet auto-highlight-symbol auto-compile aggressive-indent adaptive-wrap ace-window ace-link ace-jump-helm-line ac-racer ac-ispell)))
  (custom-set-faces
   ;; custom-set-faces was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   )
  )
