(defconst my-javascript-packages
  '(add-node-modules-path
    company-flow
    flycheck
    prettier-js
    rjsx-mode))

(defun my-javascript/init-rjsx-mode ()
  (use-package rjsx-mode
    :defer t
    :init
    (progn
      (with-eval-after-load 'rjsx-mode
        (define-key rjsx-mode-map "<" nil)
        (define-key rjsx-mode-map (kbd "C-d") nil))

      ;; (add-to-list 'auto-mode-alist '("\\.js\\'" . rjsx-mode))

      (setq
       js2-mode-show-strict-warnings nil
       js2-mode-show-parse-errors nil
       js-indent-level 2
       js2-basic-offset 2
       js2-strict-trailing-comma-warning nil
       js2-strict-missing-semi-warning nil
       js2-missing-semi-one-line-override nil)

      (advice-add #'js-jsx-indent-line
                  :after
                  #'my-javascript/js-jsx-indent-line-align-closing-bracket)
    ;;;:config
    ;;;(modify-syntax-entry ?_ "w" js2-mode-syntax-table)
      )))

(defun my-javascript/post-init-add-node-modules-path ()
  (with-eval-after-load 'rjsx-mode
    (add-hook 'rjsx-mode-hook #'add-node-modules-path)))

(defun my-javascript/post-init-company-flow ()
  (spacemacs|add-company-backends
    :backends
    '((company-flow :with company-dabbrev-code)
      company-files)))

(defun my-javascript/post-init-flycheck ()
  (with-eval-after-load 'flycheck
    (push 'javascript-jshint flycheck-disabled-checkers)
    (push 'json-jsonlint flycheck-disabled-checkers))
  (spacemacs/enable-flycheck 'rjsx-mode))
;; (spacemacs/add-flycheck-hook 'rjsx-mode))

(defun my-javascript/init-prettier-js ()
  (use-package prettier-js
    :defer t
    :init
    (progn
      (setq prettier-js-command "prettier-eslint")
      ;; (add-hook 'js-mode-hook 'prettier-js-mode)
      ;; (add-hook 'js2-mode-hook 'prettier-js-mode)
      ;; (add-hook 'rjsx-mode-hook 'prettier-js-mode)
      )))
