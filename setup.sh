#!/bin/bash

BASE_PATH=`dirname $(readlink -f $0)`
cd $BASE_PATH || {
    echo "failed change directory to setup, exit" > /dev/stderr
    exit 1
}

# link the files dotfiles/*
dotfiles(){
    for f in $BASE_PATH/dotfiles/*; do
        dst="$HOME/.$(basename $f)"
        cmd="ln -fs $f $dst"
        if [ -e "$dst" ]; then
            echo "$dst exists, skipped command:"
            echo "$cmd"
        else
            echo "run command: $cmd"
            eval "$cmd"
        fi
        echo
    done
}

# install the packages lies in installs/*
installs() {
    for f in $BASE_PATH/installs/*; do
        echo "install packages in $f"
        echo source $f
        echo
    done
}

eval $*
