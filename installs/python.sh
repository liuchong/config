#!/bin/bash

# upgrade pip
pip install --upgrade pip &&

pip install ipython &&
pip install pipenv &&

pip install --upgrade "jedi>=0.9.0" "json-rpc>=1.8.1" "service_factory>=0.1.5" &&
pip install flake8 &&
pip install autoflake &&
pip install hy &&
pip install yapf &&
# only python 2.7 &&
# pip install futures
pip install isort &&

# https://github.com/jorgenschaefer/elpy
# Either of these
pip install rope &&
pip install jedi &&
# flake8 for code checks
pip install flake8 &&
# importmagic for automatic imports
pip install importmagic &&
pip install epc &&
# and autopep8 for automatic PEP8 formatting
pip install autopep8 &&
# and yapf for code formatting
pip install yapf &&

# Python 3.6 to JavaScript compiler, https://github.com/qquick/Transcrypt
pip install transcrypt &&

pip install wakatime &&

pip install jep &&

pip install pgcli &&

pip install pydot &&

# For pypy2 install (build pypy2 with python2)
pip install genc &&
pip install pycparser &&

pip install thefuck &&

pip install asciinema &&

echo done
