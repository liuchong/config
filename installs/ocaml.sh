#!/bin/bash

opam install merlin &&
opam install utop &&
opam install ocp-indent &&

opam init &&
opam config setup -a &&

echo done
