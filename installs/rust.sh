#!/bin/bash

# curl https://sh.rustup.rs -sSf | sh

rustup component add rls-preview rust-analysis rust-src &&
rustup component add rustfmt-preview &&

cargo install -f cargo-update &&
cargo install -f cargo-src &&
cargo install -f cargo-script &&
cargo install -f cargo-web &&
cargo install -f cargo-xbuild &&
cargo install -f xargo &&
cargo install -f bootimage &&
cargo install -f wasm-bindgen-cli &&
cargo install -f wasm-gc &&
cargo install -f racer &&
cargo install -f rustsym &&
cargo install -f clippy &&
cargo install -f rusty-tags &&
cargo install -f clog-cli &&
cargo install -f git-journal &&
cargo install -f tokei &&
cargo install -f hyperfine &&
cargo install -f cargo-graph &&
cargo install -f diesel_cli &&
cargo install -f ripgrep &&
cargo install -f fd-find &&
cargo install -f exa &&
cargo install -f mdbook &&
cargo install -f itm && # --vers 0.2.1

cargo install -f d &&

echo done
