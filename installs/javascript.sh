#!/bin/bash

# management
npm install -g npm &&
npm install -g yarn &&
npm install -g prebuild &&

npm install -g webpack &&
npm install -g lerna &&
npm install -g parcel-bundler &&

npm install -g uglify-js &&
npm install -g uglify-es &&

# lsp
npm install -g typescript &&
npm install -g javascript-typescript-langserver &&
npm install -g flow-language-server &&
npm install -g typescript-language-server &&

# repository
##npm install -g sinopia &&
# https://cnpmjs.org/
npm install -g cnpm --registry=https://registry.npm.taobao.org &&

# code-analysis engine
npm install -g tern &&
npm install -g tern-jsx &&

# test
npm install -g jest &&
npm install -g mocha &&
npm install -g jasmine &&
npm install -g istanbul &&
npm install -g cucumber &&

# all about standard/eslint
npm install -g standard &&
npm install -g standard-eject &&
npm install -g eslint &&
npm install -g babel-eslint &&
npm install -g eslint-config-standard &&
npm install -g eslint-config-standard-jsx &&
npm install -g eslint-config-standard-react &&
npm install -g eslint-config-prettier &&
npm install -g eslint-plugin-prettier &&
npm install -g eslint-plugin-node &&
npm install -g eslint-plugin-promise &&
npm install -g eslint-plugin-standard &&
npm install -g eslint-plugin-flowtype &&
npm install -g eslint-plugin-typescript &&
npm install -g eslint-plugin-markdown &&
npm install -g eslint-plugin-html &&
npm install -g snazzy &&
npm install -g standard-engine &&
npm install -g semistandard &&

# more eslint packages
npm install -g eslint-plugin-graphql &&
npm install -g eslint-plugin-jest &&
npm install -g eslint-plugin-mocha &&
npm install -g eslint-plugin-jasmine &&
npm install -g eslint-config-airbnb &&
npm install -g eslint-config-react-app &&
npm install -g eslint-loader &&
npm install -g eslint-plugin-import &&
npm install -g eslint-plugin-jsx-a11y@5.1.1 &&
npm install -g eslint-plugin-react &&

# other lint
npm install -g tslint &&
npm install -g csslint &&
npm install -g stylelint &&
npm install -g stylelint-config-standard &&
npm install -g stylelint-config-styled-components &&
npm install -g stylelint-processor-styled-components &&

# format
npm install -g prettier &&
npm install -g prettier-eslint-cli &&
npm install -g js-beautify &&
npm install -g typescript-formatter &&
# https://github.com/morishitter/stylefmt/issues/334#issuecomment-379341106
# npm install -g stylefmt &&
npm install -g https://github.com/kennyinthirathclq/stylefmt.git

# SLIME REPL and other development tools for in-browser JavaScript and Node.JS
npm install -g swank-js &&

# framework
npm install -g create-react-app &&
npm install -g create-react-native-app &&
npm install -g react-native-cli &&
npm install -g @angular/cli &&
npm install -g vue-cli &&
npm install -g nw &&
# npm install -g nativescript &&
npm install -g apollo-codegen &&

# UI
npm i -g @storybook/cli &&

# css/html tools
npm install -g stylus &&
npm install -g handlebars &&
npm install -g postcss-cli &&

# lang compile to JS support
npm install -g tsun &&
npm install -g ts-node &&
npm install -g dts-gen &&
npm install -g flow-bin &&
npm install -g flow-remove-types &&
npm install -g lumo-cljs &&
npm install -g shadow-cljs &&
npm install -g purescript &&
npm install -g elm &&
npm install -g elm-oracle &&
npm install -g elm-format@exp &&
npm install -g elm-test &&

# bindings
npm install -g neon-cli &&

# macros
npm install -g @sweet-js/cli &&

# validation
npm install -g ajv-cli &&

# db
npm install -g sql-generate &&
npm install -g pg &&
npm install -g mysql &&

# npm tools
npm install -g npm-check-updates &&
npm install -g np &&
npm install -g node-gyp &&
npm install -g node-pre-gyp &&
npm install -g nw-gyp &&

# logs
npm install -g pino-tee &&

# dev tools
npm install -g nodemon &&
npm install -g iron-node &&
npm install -g documentation &&
npm install -g typedoc &&
npm install -g react-devtools &&
npm install -g graphql-faker &&
npm install -g electron &&
npm install -g vmd &&
npm install -g markdown &&
npm install -g quicktype &&

# serverless
npm install -g serverless &&
npm install -g graphcool &&
npm install -g graphql-cli &&
npm install -g get-graphql-schema &&
npm install -g graphql-up &&

# out of the box
npm install -g postgraphql &&

# other
npm install -g json &&
npm install -g gitbook-cli &&
npm install -g hexo-cli &&
npm install -g gatsby-cli &&
npm install -g gh-badges &&
npm install -g shelljs &&
npm install -g shx &&
npm install -g now &&
npm install -g surge &&
npm install -g editorconfig &&
npm install -g http-server &&
npm install -g serve &&
npm install -g lite-server &&

# notices

echo \
     "NEED install tidy-html5 from https://github.com/htacg/tidy-html5"\
    >/dev/stderr
